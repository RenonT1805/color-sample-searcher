package search

import (
	"image"
	"image/color"

	"gocv.io/x/gocv"
)

type RGB struct {
	R int `json:"R"`
	G int `json:"G"`
	B int `json:"B"`
}

type SearchResult struct {
	ColorAverage RGB             `json:"color_average"`
	Rect         image.Rectangle `json:"rect"`
}

func judge(mat gocv.Mat, row, col, border int) bool {
	if row < 0 || row >= mat.Rows() ||
		col < 0 || col >= mat.Cols() {
		return false
	}
	if mat.GetUCharAt(row, col) > uint8(border) {
		return true
	}
	return false
}

func evaluation(mat gocv.Mat, border int) int {
	count := 0
	for row := 0; row < mat.Rows(); row++ {
		for col := 0; col < mat.Cols(); col++ {
			if judge(mat, row, col, border) {
				count++
			}
		}
	}
	return count
}

func searchColorSample(mat *gocv.Mat, region image.Rectangle, unitSize, border int) image.Rectangle {
	rects := make([]image.Rectangle, unitSize*unitSize)
	rowUnit := region.Size().Y / unitSize
	colUnit := region.Size().X / unitSize
	for row := 0; row < unitSize; row++ {
		for col := 0; col < unitSize; col++ {
			rects = append(rects, image.Rect(
				colUnit*col, rowUnit*row, colUnit*(col+1), rowUnit*(row+1),
			))
		}
	}

	var maxV, maxI int
	for i, rect := range rects {
		tmp := mat.Region(rect)
		av := evaluation(tmp, border)
		if av > maxV {
			maxV = av
			maxI = i
		}
	}

	return rects[maxI]
}

func matAverage(mat gocv.Mat) (r, g, b int) {
	for row := 0; row < mat.Rows(); row++ {
		for col := 0; col < mat.Cols()*3; col += 3 {
			b += int(mat.GetUCharAt(row, col))
			g += int(mat.GetUCharAt(row, col+1))
			r += int(mat.GetUCharAt(row, col+2))
		}
	}
	b /= mat.Cols() * mat.Rows()
	g /= mat.Cols() * mat.Rows()
	r /= mat.Cols() * mat.Rows()
	return
}

func Search(sourcePath, procImagePath string, split, border int) SearchResult {
	mat := gocv.IMRead(sourcePath, gocv.IMReadColor)

	grayMat := gocv.NewMat()
	gocv.CvtColor(mat, &grayMat, gocv.ColorBGRToGray)

	rect := searchColorSample(&grayMat, image.Rect(0, 0, mat.Cols(), mat.Rows()), split, border)

	regionMat := mat.Region(rect)
	r, g, b := matAverage(regionMat)

	if procImagePath != "" {
		gocv.Rectangle(&mat, rect, color.RGBA{R: 255, G: 0, B: 0, A: 0}, 2)
		gocv.IMWrite(procImagePath, mat)
	}

	return SearchResult{
		ColorAverage: RGB{
			R: r,
			G: g,
			B: b,
		},
		Rect: rect,
	}
}
