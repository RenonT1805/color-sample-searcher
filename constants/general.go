package constants

// AppName : このアプリケーションの名前
const AppName = "ColorSampleSearcher"

// LowerAppName : 小文字にしたこのアプリケーションの名前
const LowerAppName = "color-sample-searcher"

// Version : このアプリケーションのバージョン
const Version = "v0.0.1"
