package constants

type ResponseCode int

const (
	Successful ResponseCode = iota
	Failed
	NotFound
)
