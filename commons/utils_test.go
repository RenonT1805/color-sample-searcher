package commons_test

import (
	"color-sample-searcher/commons"
	"reflect"
	"testing"
)

func TestToStruct(t *testing.T) {
	t.Parallel()
	type target struct {
		S string `json:"s"`
		I int    `json:"i"`
		B bool   `json:"b"`
		A []int  `json:"a"`
	}
	type aryArgs struct {
		source interface{}
		result []target
	}
	argsTests := []struct {
		name string
		args aryArgs
		want interface{}
	}{
		{
			name: "配列struct",
			args: aryArgs{
				source: []map[string]interface{}{
					{
						"s": "hoge",
						"i": 12,
						"b": true,
						"a": []int{1, 2, 3},
					},
					{
						"s": "fuga",
						"i": 18,
						"b": false,
						"a": []int{4, 5, 6},
					},
				},
				result: []target{},
			},
			want: []target{
				{
					S: "hoge",
					I: 12,
					B: true,
					A: []int{1, 2, 3},
				},
				{
					S: "fuga",
					I: 18,
					B: false,
					A: []int{4, 5, 6},
				},
			},
		},
	}
	for _, tt := range argsTests {
		t.Run(tt.name, func(t *testing.T) {
			if err := commons.ToStruct(tt.args.source, &tt.args.result); err != nil {
				t.Error(err)
			}
			if !reflect.DeepEqual(tt.args.result, tt.want) {
				t.Errorf("ToStruct() = %v, want %v", tt.args.result, tt.want)
			}
		})
	}

	type args struct {
		source interface{}
		result target
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{
			name: "struct",
			args: args{
				source: map[string]interface{}{
					"s": "hoge",
					"i": 12,
					"b": true,
					"a": []int{1, 2, 3},
				},
				result: target{},
			},
			want: target{
				S: "hoge",
				I: 12,
				B: true,
				A: []int{1, 2, 3},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := commons.ToStruct(tt.args.source, &tt.args.result); err != nil {
				t.Error(err)
			}
			if !reflect.DeepEqual(tt.args.result, tt.want) {
				t.Errorf("ToStruct() = %v, want %v", tt.args.result, tt.want)
			}
		})
	}
}

func TestStringArrayContains(t *testing.T) {
	t.Parallel()
	type args struct {
		target string
		list   []string
	}
	argsTests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "配列に文字列が含まれる",
			args: args{
				target: "hoge",
				list:   []string{"hoge", "fuga"},
			},
			want: true,
		},
		{
			name: "配列に文字列が含まれない",
			args: args{
				target: "hoge",
				list:   []string{"aaa", "fuga"},
			},
			want: false,
		},
		{
			name: "配列が空",
			args: args{
				target: "hoge",
				list:   []string{},
			},
			want: false,
		},
	}
	for _, tt := range argsTests {
		t.Run(tt.name, func(t *testing.T) {
			if got := commons.StringArrayContains(tt.args.target, tt.args.list); got != tt.want {
				t.Errorf("StringArrayContains() = %v, want %v", got, tt.want)
			}
		})
	}
}
