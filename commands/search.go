package commands

import (
	"color-sample-searcher/constants"
	"color-sample-searcher/search"
	"errors"
	"flag"
	"fmt"

	log "github.com/sirupsen/logrus"
)

// Search : サーチコマンド
type Search struct {
	Flags      *flag.FlagSet
	outImage   string
	sourcePath string
	outPath    string
	split      int
	border     int
}

func (s Search) showCommandUsage() {
	out := log.StandardLogger().Out
	flag.CommandLine.SetOutput(out)
	fmt.Fprintf(out, "\n使い方: %s %s [options...]\n", constants.LowerAppName, s.CommandName())
	fmt.Fprintf(out, "\n説明: %s\n", s.CommandDescription())
	fmt.Fprintf(out, "\n実行オプション:\n")
	s.Flags.PrintDefaults()
}

// initialize : コマンドの初期化
func (s *Search) initialize(args []string) error {
	s.Flags = flag.NewFlagSet(s.CommandName(), flag.ExitOnError)
	s.Flags.Usage = s.showCommandUsage
	s.Flags.StringVar(&s.outPath, "out", "", "処理後の情報の出力先ファイルを指定します")
	s.Flags.StringVar(&s.outImage, "out_img", "", "処理後の画像出力先ファイルを指定します")
	s.Flags.StringVar(&s.sourcePath, "source", "", "処理対象の画像ファイルを指定します（必須）")
	s.Flags.IntVar(&s.split, "split", 32, "画像の分割単位を指定します。高いほど細かい単位で画像を判定します")
	s.Flags.IntVar(&s.border, "border", 240, "画像の色情報のしきい値を指定します。この値より大きな色が多い部分を調査します")
	err := s.Flags.Parse(args)
	if err != nil {
		return err
	}

	if s.sourcePath == "" {
		return errors.New("sourceの指定は必須です")
	}
	return nil
}

// CommandName : コマンド名を返す
func (s Search) CommandName() string {
	return "search"
}

// CommandDescription : コマンド説明を返す
func (s Search) CommandDescription() string {
	return "渡されたjpegファイルから色見本の位置を特定します"
}

// Run : コマンドの実行
func (s Search) Run(args []string) constants.ResponseCode {
	err := s.initialize(args)
	if err != nil {
		log.Errorln(err)
		return constants.Failed
	}

	result := search.Search(s.sourcePath, s.outImage, s.split, s.border)
	fmt.Println(result)

	return constants.Successful
}
