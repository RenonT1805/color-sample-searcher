package commands

import (
	"color-sample-searcher/constants"
	"flag"
	"fmt"
	"os"

	"github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
)

const commandDescription = "画像の色見本の位置を探すツール"

// Command : コマンドのインターフェース
type Command interface {
	CommandName() string
	CommandDescription() string
	Run([]string) constants.ResponseCode
}

// Options : ColorSampleSearcherのオプション
type Options struct {
	Commands []Command
	Version  bool
}

var options Options

func init() {
	log.SetFormatter(&log.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	})
	log.SetOutput(colorable.NewColorableStdout())

	flag.CommandLine.Init(constants.AppName, flag.ExitOnError)
	flag.CommandLine.Usage = showCommandUsage

	options.Commands = GetAllCommands()

	flag.BoolVar(&options.Version, "v", false, "バージョン情報を表示します")
}

func showCommandUsage() {
	out := log.StandardLogger().Out
	flag.CommandLine.SetOutput(out)
	fmt.Fprintf(out, "\n使い方: %s\n", constants.AppName)
	fmt.Fprintf(out, "\n説明: %s\n", commandDescription)
	fmt.Fprintf(out, "\n実行オプション:\n")
	flag.PrintDefaults()
	fmt.Fprintf(out, "\nコマンド:\n")
	for _, command := range options.Commands {
		fmt.Printf("  %s\t%s\n", command.CommandName(), command.CommandDescription())
	}
	fmt.Fprintf(out, "\n")
}

// GetAllCommands : すべてのコマンドのインスタンスを取得する
func GetAllCommands() []Command {
	commands := []Command{
		&Search{},
		&Version{},
	}
	return commands
}

// RunCommands : コマンドライン引数からコマンドを判定し、実行する
func RunCommands(args []string, ops Options) constants.ResponseCode {
	if len(args) == 0 {
		showCommandUsage()
		return constants.Successful
	}
	for _, command := range ops.Commands {
		if command.CommandName() == args[0] {
			return command.Run(args[1:])
		}
	}
	return constants.NotFound
}

// CommandsEntryPoint : このアプリケーションのエントリーポイント
func CommandsEntryPoint() {
	flag.Parse()
	args := flag.Args()

	// Versionコマンドのシンタックスシュガーである -v をここで処理している
	if options.Version {
		args = []string{"version"}
	}

	result := RunCommands(args, options)
	if result == constants.NotFound {
		log.Printf("%s は存在しないコマンドです\n", args[0])
		showCommandUsage()
	}
	os.Exit(int(result))
}
